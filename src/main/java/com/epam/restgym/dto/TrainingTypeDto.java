package com.epam.restgym.dto;

import com.epam.restgym.model.TrainingTypeName;

public record TrainingTypeDto(
        Long id,
        TrainingTypeName trainingTypeName
) {
}
