package com.epam.restgym.controller;

import com.epam.restgym.dto.TrainingTypeDto;
import com.epam.restgym.model.Coach;
import com.epam.restgym.model.Training;
import com.epam.restgym.model.TrainingTypeName;
import com.epam.restgym.service.TrainingService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestControllerConfiguration.class})
@WebAppConfiguration
class TrainingControllerTest {

    private MockMvc mockMvc;

    @Autowired
    TrainingController trainingController;

    @Autowired
    TrainingService trainingService;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(trainingController).build();
    }

    @Test
    void getNotAssignedOnTraineeCoaches() throws Exception {
        var list = Arrays.asList(new Coach(), new Coach(), new Coach(), new Coach());
        when(trainingService.getNotAssignedOnTraineeCoaches(anyString())).thenReturn(list);

        mockMvc.perform(get("/trainings/coach/username232"))
                .andExpect(jsonPath("$", Matchers.hasSize(4)))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void getTraineeTrainings() throws Exception {
        var request = "{\n" +
                "  \"username\": \"john_doe\",\n" +
                "  \"periodFrom\": \"2023-01-01\",\n" +
                "  \"periodTo\": \"2023-12-31\",\n" +
                "  \"coachUsername\": \"coach_user\",\n" +
                "  \"trainingType\": {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"BOXING\"\n" +
                "  }\n" +
                "}\n";

        var list = Arrays.asList(new Training(), new Training(), new Training());
        when(trainingService.getTraineeTrainings(any())).thenReturn(list);

        mockMvc.perform(get("/trainings/trainee").contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(jsonPath("$", Matchers.hasSize(3)))
                .andExpect(status().isOk());
    }

    @Test
    void getCoachTrainings() throws Exception {
        var request = "{\n" +
                "  \"username\": \"coach_user\",\n" +
                "  \"periodFrom\": \"2023-01-01\",\n" +
                "  \"periodTo\": \"2023-12-31\",\n" +
                "  \"traineeUsername\": \"john_doe\"\n" +
                "}\n";

        var list = Arrays.asList(new Training(), new Training(), new Training(), new Training(), new Training(), new Training());

        when(trainingService.getCoachTrainings(any())).thenReturn(list);

        mockMvc.perform(get("/trainings/coach").contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(jsonPath("$", Matchers.hasSize(6)))
                .andExpect(status().isOk());
    }

    @Test
    void addTraining() throws Exception {
        var request = "{\n" +
                "  \"traineeUsername\": \"john_doe\",\n" +
                "  \"coachUsername\": \"coach_user\",\n" +
                "  \"trainingName\": \"Workout Session\",\n" +
                "  \"trainingDate\": \"2023-01-15\",\n" +
                "  \"durationInMinutes\": 60\n" +
                "}";

        doNothing().when(trainingService).addTraining(any());

        mockMvc.perform(post("/trainings").contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(jsonPath("$.message").value("training added successfully"))
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(status().isOk());
    }

    @Test
    void getTrainingTypes() throws Exception {
        var list = Arrays.asList(
                new TrainingTypeDto(1L, TrainingTypeName.KARATE),
                new TrainingTypeDto(2L, TrainingTypeName.JUJITSU),
                new TrainingTypeDto(3L, TrainingTypeName.SWIMMING),
                new TrainingTypeDto(4L, TrainingTypeName.WORKOUT),
                new TrainingTypeDto(5L, TrainingTypeName.BOXING),
                new TrainingTypeDto(6L, TrainingTypeName.BODYBUILDING),
                new TrainingTypeDto(7L, TrainingTypeName.FITNESS)
        );
        when(trainingService.getTrainingTypes()).thenReturn(list);

        mockMvc.perform(get("/trainings"))
                .andExpect(jsonPath("$", Matchers.hasSize(7)))
                .andExpect(status().isOk());
    }
}