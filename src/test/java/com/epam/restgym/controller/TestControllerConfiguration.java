package com.epam.restgym.controller;


import com.epam.restgym.service.CoachService;
import com.epam.restgym.service.TraineeService;
import com.epam.restgym.service.TrainingService;
import com.epam.restgym.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.mockito.Mockito.mock;

@Configuration
class TestControllerConfiguration {

    @Bean
    public CoachService coachService() {
        return mock(CoachService.class);
    }

    @Bean
    public TraineeService traineeService() {
        return mock(TraineeService.class);
    }

    @Bean
    public TrainingService trainingService() {
        return mock(TrainingService.class);
    }

    @Bean
    public UserService userService() {
        return mock(UserService.class);
    }

    @Bean
    public CoachController coachController(CoachService coachService) {
        return new CoachController(coachService);
    }

    @Bean
    public TraineeController traineeController(TraineeService traineeService) {
        return new TraineeController(traineeService);
    }

    @Bean
    public TrainingController trainingController() {
        return new TrainingController(trainingService());
    }

    @Bean
    public AuthController authController() {
        return new AuthController(userService());
    }
}
