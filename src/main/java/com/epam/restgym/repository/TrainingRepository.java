package com.epam.restgym.repository;

import com.epam.restgym.model.Coach;
import com.epam.restgym.model.Trainee;
import com.epam.restgym.model.Training;
import com.epam.restgym.model.TrainingType;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.*;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Repository
public class TrainingRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Training save(Training entity) {
        if (entityManager.contains(entity)) {
            entity = entityManager.merge(entity);
        } else {
            entityManager.persist(entity);
        }

        return entity;
    }

    public void delete(Long id) {
        entityManager.remove(id);
        entityManager.flush();
    }

    public Optional<Training> findById(Long id) {
        return Optional.ofNullable(entityManager.find(Training.class, id));
    }

    public List<Training> findAll() {
        var criteria = entityManager.getCriteriaBuilder().createQuery(Training.class);
        criteria.from(Training.class);

        return entityManager.createQuery(criteria)
                .getResultList();
    }

    public List<Coach> findActiveCoachesNotAssignedOnTrainee(String traineeUsername) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Coach> cq = cb.createQuery(Coach.class);

        Subquery<Long> assignedTrainersSubquery = cq.subquery(Long.class);
        Root<Training> assignedTrainersRoot = assignedTrainersSubquery.from(Training.class);
        Join<Training, Coach> assignedTrainersJoin = assignedTrainersRoot.join("coach", JoinType.INNER);
        Join<Training, Trainee> traineeJoin = assignedTrainersRoot.join("trainee", JoinType.INNER);
        assignedTrainersSubquery.select(assignedTrainersJoin.get("id"))
                .where(cb.equal(traineeJoin.get("user").get("username"), traineeUsername));

        Root<Coach> trainerRoot = cq.from(Coach.class);
        cq.select(trainerRoot)
                .where(
                        cb.isTrue(trainerRoot.get("user").get("isActive")),
                        cb.not(trainerRoot.get("id").in(assignedTrainersSubquery))
                );



        TypedQuery<Coach> query = entityManager.createQuery(cq);
        return query.getResultList();
    }

    public List<Training> findTrainingsOfTrainee(String traineeUsername,
                                                 LocalDate periodFrom,
                                                 LocalDate periodTo,
                                                 String trainerUsername,
                                                 TrainingType trainingType) {
        if (traineeUsername == null) return Collections.emptyList();

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Training> cq = cb.createQuery(Training.class);
        Root<Training> root = cq.from(Training.class);

        List<Predicate> predicates = new ArrayList<>();
        final Predicate predicateUsername = cb.equal(root.get("trainee").get("user").get("username"), traineeUsername);
        predicates.add(predicateUsername);

        if (trainingType != null) {
            final Predicate predicateTrainingType = cb.equal(root.get("trainingType"), trainingType);
            predicates.add(predicateTrainingType);
        }

        if (trainerUsername != null) {
            final Predicate predicateTrainerUsername = cb.equal(root.get("coach").get("user").get("username"), trainerUsername);
            predicates.add(predicateTrainerUsername);
        }

        if (periodFrom != null) {
            Predicate predicatePeriodFrom = cb.greaterThan(root.get("trainingDate"), periodFrom);
            predicates.add(predicatePeriodFrom);
        }

        if (periodTo != null) {
            final Predicate predicatePeriodTo = cb.lessThan(root.get("trainingDate"), periodTo);
            predicates.add(predicatePeriodTo);
        }

        cq.select(root).where(predicates.toArray(Predicate[]::new));

        return entityManager.createQuery(cq).getResultList();
    }

    public List<Training> findTrainingsOfCoach(String trainerUsername,
                                               LocalDate periodFrom,
                                               LocalDate periodTo,
                                               String traineeUsername) {

        if (trainerUsername == null) return Collections.emptyList();

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Training> cq = cb.createQuery(Training.class);
        Root<Training> root = cq.from(Training.class);

        List<Predicate> predicates = new ArrayList<>();

        final Predicate predicateUsername = cb.equal(root.get("coach").get("user").get("username"), trainerUsername);
        predicates.add(predicateUsername);

        if (traineeUsername != null) {
            final Predicate predicateTraineeUsername = cb.equal(root.get("trainee").get("user").get("username"), traineeUsername);
            predicates.add(predicateTraineeUsername);
        }
        if (periodFrom != null) {
            final Predicate predicatePeriodFrom = cb.greaterThan(root.get("trainingDate"), periodFrom);
            predicates.add(predicatePeriodFrom);
        }
        if (periodTo != null) {
            final Predicate predicatePeriodTo = cb.lessThan(root.get("trainingDate"), periodTo);
            predicates.add(predicatePeriodTo);
        }

        cq.select(root).where(predicates.toArray(Predicate[]::new));

        return entityManager.createQuery(cq).getResultList();
    }
}
