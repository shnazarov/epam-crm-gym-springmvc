package com.epam.restgym.service;

import com.epam.restgym.dto.AddTrainingRequest;
import com.epam.restgym.dto.GetTrainingsOfTraineeRequest;
import com.epam.restgym.dto.GetTrainingsOfCoachRequest;
import com.epam.restgym.dto.TrainingTypeDto;
import com.epam.restgym.exception.DataNotFoundException;
import com.epam.restgym.model.Coach;
import com.epam.restgym.model.Training;
import com.epam.restgym.model.TrainingType;
import com.epam.restgym.repository.CoachRepository;
import com.epam.restgym.repository.TraineeRepository;
import com.epam.restgym.repository.TrainingRepository;
import com.epam.restgym.repository.TrainingTypeRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class TrainingService {


    private TrainingRepository trainingRepository;
    private TraineeRepository traineeRepository;
    private CoachRepository coachRepository;
    private TrainingTypeRepository trainingTypeRepository;


    public void addTraining(AddTrainingRequest request) {
        var trainee = traineeRepository.getTraineeByUsername(request.getTraineeUsername())
                .orElseThrow(() -> new DataNotFoundException("Trainee", request.getTraineeUsername()));
        var coach = coachRepository.getCoachByUsername(request.getCoachUsername())
                .orElseThrow(() -> new DataNotFoundException("Coach", request.getCoachUsername()));


        var training = Training.builder()
                .trainingName(request.getTrainingName())
                .trainingType(coach.getSpecialization())
                .trainingDate(request.getTrainingDate())
                .coach(coach)
                .trainee(trainee)
                .durationInMinutes(request.getDurationInMinutes())
                .build();

        trainingRepository.save(training);
    }

    public List<Coach> getNotAssignedOnTraineeCoaches(String username) {
        return trainingRepository.findActiveCoachesNotAssignedOnTrainee(username);
    }

    public List<TrainingTypeDto> getTrainingTypes() {
        return trainingTypeRepository
                .findAll()
                .stream()
                .map(it -> new TrainingTypeDto(it.getId(), it.getTrainingTypeName()))
                .toList();
    }

    public List<Training> getTraineeTrainings(GetTrainingsOfTraineeRequest request) {
        return trainingRepository.findTrainingsOfTrainee(
                request.username(),
                request.periodFrom(),
                request.periodTo(),
                request.coachUsername(),
                new TrainingType(request.trainingType().id(), request.trainingType().trainingTypeName())
        );
    }

    public List<Training> getCoachTrainings(GetTrainingsOfCoachRequest request) {
        return trainingRepository.findTrainingsOfCoach(
                request.username(),
                request.periodFrom(),
                request.periodTo(),
                request.traineeUsername()
        );
    }
}
