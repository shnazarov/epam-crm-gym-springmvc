package com.epam.restgym.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;


@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.epam.restgym.controller"})
public class WebConfig implements WebMvcConfigurer {

}
