package com.epam.restgym.controller;

import com.epam.restgym.dto.*;
import com.epam.restgym.model.Coach;
import com.epam.restgym.service.CoachService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/coaches")
@AllArgsConstructor
public class CoachController {

    private final CoachService coachService;

    @PostMapping
    public ResponseEntity<RegisterResponse> registerCoach(@RequestBody RegisterCoachRequest request) {
        var response = coachService.registerCoach(request);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{username}")
    public ResponseEntity<Coach> coachProfile(@PathVariable(name = "username") String username) {
        var response = coachService.getCoachByUsername(username);
        return ResponseEntity.ok(response);
    }

    @PutMapping
    public ResponseEntity<Coach> updateCoach(@RequestBody UpdateCoachProfileRequest request) {
        var response = coachService.updateTrainerProfile(request);
        return ResponseEntity.ok(response);
    }

    @PatchMapping
    public ResponseEntity<ApiResponse> activateDeactivateCoach(@RequestBody ActivateDeactivate activateDeactivate) {
        coachService.activateDeactivateCoach(activateDeactivate);
        boolean isActive = activateDeactivate.isActive();
        var response = ApiResponse.builder()
                .message(isActive ? "coach activated successfully" : "coach deactivated successfully")
                .success(true)
                .build();

        return ResponseEntity.ok(response);
    }
}
