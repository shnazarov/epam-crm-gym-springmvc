package com.epam.restgym.service;

import com.epam.restgym.dto.ChangePasswordRequest;
import com.epam.restgym.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public void changeUserPassword(ChangePasswordRequest request) {
        var user = userRepository.getUserByUsername(request.getUsername());

        user.ifPresent(it -> {
            if (request.getOldPassword().equals(it.getPassword())) {
                it.setPassword(request.getNewPassword());
            }
        });
    }
}
