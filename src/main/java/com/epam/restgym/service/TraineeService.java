package com.epam.restgym.service;

import com.epam.restgym.dto.ActivateDeactivate;
import com.epam.restgym.dto.RegisterResponse;
import com.epam.restgym.dto.RegisterTraineeRequest;
import com.epam.restgym.dto.UpdateTraineeProfileRequest;
import com.epam.restgym.exception.DataNotFoundException;
import com.epam.restgym.model.Trainee;
import com.epam.restgym.model.User;
import com.epam.restgym.repository.TraineeRepository;
import com.epam.restgym.repository.UserRepository;
import com.epam.restgym.util.ProfileGeneratorUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class TraineeService {

    private TraineeRepository traineeRepository;
    private UserRepository userRepository;

    public RegisterResponse registerTrainee(RegisterTraineeRequest request) {

        var username = ProfileGeneratorUtil.usernameGenerator(request.getFirstname(), request.getLastname());

        while (userRepository.getUserByUsername(username).isPresent()) {
            username = ProfileGeneratorUtil.usernameGeneratorWithSerialNumber(username);
        }

        var password = ProfileGeneratorUtil.passwordGenerator();
        var user = User.builder()
                .firstname(request.getFirstname())
                .lastname(request.getLastname())
                .username(username)
                .password(password)
                .isActive(true)
                .build();


        var savedUser = userRepository.save(user);

        var trainee = Trainee.builder()
                .user(savedUser)
                .dateOfBirth(request.getDateOfBirth())
                .build();
        var savedTrainee = traineeRepository.save(trainee);


        return new RegisterResponse(savedTrainee.getUser().getUsername(), savedTrainee.getUser().getPassword());
    }

    public Trainee updateTraineeProfile(UpdateTraineeProfileRequest request) {
        var username = request.getUsername();

        var trainee = traineeRepository.getTraineeByUsername(username)
                .orElseThrow(() -> new DataNotFoundException("Trainee", username));

        trainee.getUser().setFirstname(request.getFirstname());
        trainee.getUser().setLastname(request.getLastname());
        trainee.setDateOfBirth(request.getDateOfBirth());

        return traineeRepository.save(trainee);
    }

    public Trainee getTraineeByUsername(String username) {
        return traineeRepository.getTraineeByUsername(username)
                .orElseThrow(() -> new DataNotFoundException("Trainee", username));
    }

    public void deleteTrainee(String username) {
        var traineeOptional = traineeRepository.getTraineeByUsername(username);
        traineeOptional.ifPresent(traineeRepository::delete);
    }

    public void activateDeactivateTrainee(ActivateDeactivate request) {
        var trainee = traineeRepository.getTraineeByUsername(request.username())
                .orElseThrow(() -> new DataNotFoundException("Coach", request.username()));

        trainee.getUser().setIsActive(request.isActive());
    }
}
