package com.epam.restgym.controller;

import com.epam.restgym.dto.ChangePasswordRequest;
import com.epam.restgym.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestControllerConfiguration.class})
@WebAppConfiguration
class AuthControllerTest {
    private MockMvc mockMvc;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthController authController;


    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(authController).build();
    }

    @Test
    void changePassword() throws Exception {
        var request = "{\n" +
                "  \"username\": \"John.Doe\",\n" +
                "  \"oldPassword\": \"oldSecret123\",\n" +
                "  \"newPassword\": \"newSecret456\"\n" +
                "}\n";

        doNothing().when(userService).changeUserPassword(any(ChangePasswordRequest.class));

        mockMvc.perform(put("/auth").contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(jsonPath("$.message").value("password changed successfully"))
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(status().isOk());
    }
}