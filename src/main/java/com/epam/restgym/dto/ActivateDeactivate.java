package com.epam.restgym.dto;

public record ActivateDeactivate(
        String username,
        Boolean isActive
) {

}
