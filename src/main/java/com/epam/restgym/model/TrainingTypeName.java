package com.epam.restgym.model;

public enum TrainingTypeName {
    BODYBUILDING,
    SWIMMING,
    FITNESS,
    WORKOUT,
    BOXING,
    TAEKWONDO,
    KARATE,
    JUJITSU
}
