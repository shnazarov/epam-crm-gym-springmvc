package com.epam.restgym.controller;

import com.epam.restgym.dto.ActivateDeactivate;
import com.epam.restgym.dto.RegisterCoachRequest;
import com.epam.restgym.dto.RegisterResponse;
import com.epam.restgym.dto.UpdateCoachProfileRequest;
import com.epam.restgym.model.Coach;
import com.epam.restgym.service.CoachService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestControllerConfiguration.class})
@WebAppConfiguration
class CoachControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private CoachController coachController;

    @Autowired
    private CoachService coachService;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(coachController).build();
    }


    @Test
    void registerCoach() throws Exception {

        when(coachService.registerCoach(any(RegisterCoachRequest.class)))
                .thenReturn(any(RegisterResponse.class));

        String request = "{" +
                "  \"firstname\": \"John\"," +
                "  \"lastname\": \"Doe\"," +
                "  \"specialization\": {" +
                "    \"id\": 1," +
                "    \"trainingTypeName\": \"BOXING\"" +
                "  }" +
                "}\n";

        mockMvc.perform(post("/coaches").contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isOk());

        verify(coachService, times(1)).registerCoach(any(RegisterCoachRequest.class));
    }
    @Test
    void coachProfile() throws Exception {
        when(coachService.getCoachByUsername(anyString())).thenReturn(new Coach());

        mockMvc.perform(get("/coaches/test"))
                .andExpect(status().isOk());

        verify(coachService, times(1)).getCoachByUsername(anyString());
    }



    @Test
    void updateCoach() throws Exception {
        when(coachService.updateTrainerProfile(any(UpdateCoachProfileRequest.class)))
                .thenReturn(any(Coach.class));

        String request = "{" +
                "  \"firstname\": \"John\"," +
                "  \"lastname\": \"Doe\"," +
                "   \"username\":\"John.Doe\"," +
                "  \"specialization\": {" +
                "    \"id\": 1," +
                "    \"trainingTypeName\": \"BOXING\"" +
                "  }" +
                "}";

        mockMvc.perform(put("/coaches").contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isOk());
        verify(coachService, times(1)).updateTrainerProfile(any(UpdateCoachProfileRequest.class));
    }

    @Test
    void activateDeactivateCoach() throws Exception {
        var activate = new ActivateDeactivate("John.Doe", true);

        doNothing().when(coachService).activateDeactivateCoach(activate);

        var requestActivate = "{" +
                "  \"username\": \"John.Doe\"," +
                "  \"isActive\": true" +
                "}";

        mockMvc.perform(patch("/coaches").contentType(MediaType.APPLICATION_JSON)
                .content(requestActivate))
                .andExpect(jsonPath("$.message").value("coach activated successfully"))
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(status().isOk());


        var deactivate = new ActivateDeactivate("John.Doe", false);

        doNothing().when(coachService).activateDeactivateCoach(deactivate);

        var requestDeactivate = "{" +
                "  \"username\": \"John.Doe\"," +
                "  \"isActive\": false" +
                "}";

        mockMvc.perform(patch("/coaches").contentType(MediaType.APPLICATION_JSON)
                .content(requestDeactivate))
                .andExpect(jsonPath("$.message").value("coach deactivated successfully"))
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(status().isOk());
    }

}
