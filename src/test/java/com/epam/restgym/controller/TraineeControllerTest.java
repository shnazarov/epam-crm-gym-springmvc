package com.epam.restgym.controller;

import com.epam.restgym.dto.ActivateDeactivate;
import com.epam.restgym.dto.RegisterResponse;
import com.epam.restgym.dto.RegisterTraineeRequest;
import com.epam.restgym.dto.UpdateTraineeProfileRequest;
import com.epam.restgym.model.Trainee;
import com.epam.restgym.service.TraineeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestControllerConfiguration.class})
@WebAppConfiguration
class TraineeControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private TraineeController traineeController;

    @Autowired
    private TraineeService traineeService;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(traineeController).build();
    }

    @Test
    void traineeRegister() throws Exception {
        var request = "{\n" + "  \"firstname\": \"John\",\n" + "  \"lastname\": \"Doe\",\n" + "  \"dateOfBirth\": \"1990-01-15\",\n" + "  \"address\": \"123 Main Street\"\n" + "}";

        var serviceResult = new RegisterResponse("John.Doe", "1234pass");
        when(traineeService.registerTrainee(any(RegisterTraineeRequest.class))).thenReturn(serviceResult);

        mockMvc.perform(post("/trainees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(jsonPath("$.username").value("John.Doe"))
                .andExpect(jsonPath("$.password").value("1234pass"))
                .andExpect(status().isOk());
    }

    @Test
    void traineeProfile() throws Exception {
        when(traineeService.getTraineeByUsername(anyString())).thenReturn(any(Trainee.class));

        mockMvc.perform(get("/trainees/username111"))
                .andExpect(status().isOk());
    }

    @Test
    void updateTraineeProfile() throws Exception {
        when(traineeService.updateTraineeProfile(any(UpdateTraineeProfileRequest.class)))
                .thenReturn(any(Trainee.class));

        var request = "{\n" +
                "  \"username\": \"john.doe\",\n" +
                "  \"firstname\": \"John\",\n" +
                "  \"lastname\": \"Doe\",\n" +
                "  \"dateOfBirth\": \"1990-01-15\",\n" +
                "  \"address\": \"123 Main Street\"\n" +
                "}";

        mockMvc.perform(put("/trainees").contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isOk());
    }

    @Test
    void deleteTraineeProfile() throws Exception {
        doNothing().when(traineeService).deleteTrainee(anyString());

        mockMvc.perform(delete("/trainees/username12"))
                .andExpect(jsonPath("$.message").value("trainee deleted successfully"))
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(status().isOk());
    }

    @Test
    void activateDeactivateTrainee() throws Exception {

        var activate = new ActivateDeactivate("John.Doe", true);

        doNothing().when(traineeService).activateDeactivateTrainee(activate);

        var requestActivate = "{" +
                "  \"username\": \"John.Doe\"," +
                "  \"isActive\": true" +
                "}";

        mockMvc.perform(patch("/trainees").contentType(MediaType.APPLICATION_JSON)
                        .content(requestActivate))
                .andExpect(jsonPath("$.message").value("trainee activated successfully"))
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(status().isOk());


        var deactivate = new ActivateDeactivate("John.Doe", false);

        doNothing().when(traineeService).activateDeactivateTrainee(deactivate);

        var requestDeactivate = "{" +
                "  \"username\": \"John.Doe\"," +
                "  \"isActive\": false" +
                "}";

        mockMvc.perform(patch("/trainees").contentType(MediaType.APPLICATION_JSON)
                        .content(requestDeactivate))
                .andExpect(jsonPath("$.message").value("trainee deactivated successfully"))
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(status().isOk());
    }
}