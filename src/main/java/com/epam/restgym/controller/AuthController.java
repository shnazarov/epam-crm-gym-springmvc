package com.epam.restgym.controller;

import com.epam.restgym.dto.ApiResponse;
import com.epam.restgym.dto.ChangePasswordRequest;
import com.epam.restgym.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@AllArgsConstructor
public class AuthController {

    private final UserService userService;

    @PutMapping
    public ResponseEntity<ApiResponse> changePassword(
            @RequestBody ChangePasswordRequest request
    ) {
        userService.changeUserPassword(request);
        var response = ApiResponse.builder()
                .message("password changed successfully")
                .success(true)
                .build();

        return ResponseEntity.ok(response);
    }

}
