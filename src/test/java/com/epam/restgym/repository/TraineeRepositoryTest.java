package com.epam.restgym.repository;

import com.epam.restgym.config.HibernateConfig;
import com.epam.restgym.model.Trainee;
import com.epam.restgym.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {HibernateConfig.class})
@Sql({"classpath:schema.sql", "classpath:data.sql"})
@Transactional
class TraineeRepositoryTest {

    @Autowired
    private TraineeRepository traineeRepository;
    @Autowired
    private UserRepository userRepository;

    @Test
    void save() {
        var user = User.builder()
                .firstname("Alex")
                .lastname("Chuck")
                .username("Alex.Chuck")
                .password("alex12345")
                .isActive(true)
                .build();

        user = userRepository.save(user);

        var trainee = Trainee.builder()
                .user(user)
                .dateOfBirth(LocalDate.of(2001, 3, 8))
                .build();

        var savedTrainee = traineeRepository.save(trainee);

        Assertions.assertEquals(user.getFirstname(), savedTrainee.getUser().getFirstname());
        Assertions.assertEquals(user.getLastname(), savedTrainee.getUser().getLastname());
        Assertions.assertEquals(user.getIsActive(), savedTrainee.getUser().getIsActive());
        Assertions.assertEquals(user.getUsername(), savedTrainee.getUser().getUsername());
        Assertions.assertEquals(user.getPassword(), savedTrainee.getUser().getPassword());

        Assertions.assertEquals(trainee.getDateOfBirth(), savedTrainee.getDateOfBirth());
        Assertions.assertEquals(trainee.getTrainings().size(), savedTrainee.getTrainings().size());
    }

    @Test
    void delete() {
        var trainee = traineeRepository.findById(2L).get();
        traineeRepository.delete(trainee);
        var traineesSize = traineeRepository.findAll().size();
        Assertions.assertEquals(2, traineesSize);
    }

    @Test
    void findById() {
        var id = 1L;
        var user = userRepository.getUserByUsername("sophia_miller").get();

        var trainee = traineeRepository.findById(id).get();
        Assertions.assertEquals(user.getId(), trainee.getUser().getId());
        Assertions.assertEquals(user.getUsername(), trainee.getUser().getUsername());
        Assertions.assertEquals(user.getFirstname(), trainee.getUser().getFirstname());
        Assertions.assertEquals(user.getLastname(), trainee.getUser().getLastname());
        Assertions.assertEquals(user.getPassword(), trainee.getUser().getPassword());
        Assertions.assertEquals(user.getIsActive(), trainee.getUser().getIsActive());

        Assertions.assertEquals(LocalDate.of(1992, 5, 28), trainee.getDateOfBirth());
    }

    @Test
    void findAll() {
        var size = traineeRepository.findAll().size();
        Assertions.assertEquals(3, size);
    }

    @Test
    void getTraineeByUsername() {
        var username = "ryan_smith";
        var expectedUser = User.builder()
                .firstname("Ryan")
                .lastname("Smith")
                .username("ryan_smith")
                .password("ryan456")
                .isActive(true)
                .build();

        var expectedTrainee = Trainee.builder()
                .user(expectedUser)
                .dateOfBirth(LocalDate.of(1997, 11, 3))
                .build();

        var trainee = traineeRepository.getTraineeByUsername(username).get();
        Assertions.assertEquals(expectedUser.getFirstname(), trainee.getUser().getFirstname());
        Assertions.assertEquals(expectedUser.getLastname(), trainee.getUser().getLastname());
        Assertions.assertEquals(expectedUser.getUsername(), trainee.getUser().getUsername());
        Assertions.assertEquals(expectedUser.getIsActive(), trainee.getUser().getIsActive());
        Assertions.assertEquals(expectedUser.getPassword(), trainee.getUser().getPassword());

        Assertions.assertEquals(expectedTrainee.getDateOfBirth(), trainee.getDateOfBirth());
        Assertions.assertEquals(1, trainee.getTrainings().size());
    }
}