package com.epam.restgym.dto;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public record GetTrainingsOfTraineeRequest(
        String username,

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        LocalDate periodFrom,

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        LocalDate periodTo,
        String coachUsername,
        TrainingTypeDto trainingType
) {
}


